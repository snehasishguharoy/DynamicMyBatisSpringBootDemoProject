package com.demo.example.cru.mapper;

import java.util.List;

import com.demo.example.cru.vo.EmpVo;

public interface EmpMapperIfc {

	int empInsrt(EmpVo empVo);

	List<EmpVo> getEmpVos();

	int empUpdt(EmpVo empVo);

	EmpVo fndEmpById(Integer empId);

	Integer deleteEmp(Integer empId);

}
