package com.demo.example.cru.bo;

import java.util.List;

import com.demo.example.cru.excp.BsnObjException;
import com.demo.example.cru.vo.EmpVo;

public interface EmpBoIfc {
	Integer empInsrt(EmpVo empVo) throws BsnObjException;

	List<EmpVo> getEmpVos() throws BsnObjException;

	Integer empUpdt(EmpVo empVo) throws BsnObjException;

	EmpVo fndEmpById(Integer empId) throws BsnObjException;

	Integer deleteEmp(Integer empId) throws BsnObjException;
}
