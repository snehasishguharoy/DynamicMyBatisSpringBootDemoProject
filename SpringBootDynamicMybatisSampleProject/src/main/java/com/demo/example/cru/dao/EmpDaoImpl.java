package com.demo.example.cru.dao;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.demo.example.cru.excp.DataAcesExcp;
import com.demo.example.cru.mapper.EmpMapperIfc;
import com.demo.example.cru.vo.EmpVo;

@Repository
public class EmpDaoImpl implements EmpDaoIfc {
	private static final Logger LOGGER = Logger.getLogger(EmpDaoImpl.class.getName());
	@Autowired
	private EmpMapperIfc empMapperIfc;

	@Override
	public int empInsrt(EmpVo empVo) throws DataAcesExcp {
		LOGGER.info("Inside the Persistence Method");
		try {
			return empMapperIfc.empInsrt(empVo);
		} catch (DataAccessException dataAccessException) {
			throw new DataAcesExcp(dataAccessException);
		}
	}

	@Override
	public List<EmpVo> getEmpVos() throws DataAcesExcp {
		try {
			return empMapperIfc.getEmpVos();
		} catch (DataAccessException dataAccessException) {
			throw new DataAcesExcp(dataAccessException);
		}
	}

	@Override
	public int empUpdt(EmpVo empVo) throws DataAcesExcp {
		try {

			return empMapperIfc.empUpdt(empVo);
		} catch (DataAccessException dataAccessException) {
			throw new DataAcesExcp(dataAccessException);
		}
	}

	@Override
	public EmpVo fndEmpById(Integer empId) throws DataAcesExcp {
		try {
			return empMapperIfc.fndEmpById(empId);
		} catch (DataAccessException dataAccessException) {
			throw new DataAcesExcp(dataAccessException);
		}
	}

	@Override
	public Integer deleteEmp(Integer empId) throws DataAcesExcp {
		try{
		return empMapperIfc.deleteEmp(empId);
		}catch (DataAccessException dataAccessException) {
			throw new DataAcesExcp(dataAccessException);
		}

	}

}
