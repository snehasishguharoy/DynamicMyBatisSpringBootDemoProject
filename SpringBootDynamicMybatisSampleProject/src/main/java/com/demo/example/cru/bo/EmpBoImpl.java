package com.demo.example.cru.bo;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.example.cru.dao.EmpDaoIfc;
import com.demo.example.cru.excp.BsnObjException;
import com.demo.example.cru.excp.DataAcesExcp;
import com.demo.example.cru.vo.EmpVo;

@Service
public class EmpBoImpl implements EmpBoIfc {
	private static final Logger LOGGER = Logger.getLogger(EmpBoImpl.class.getName());
	@Autowired
	private EmpDaoIfc empDaoIfc;

	@Override
	public Integer empInsrt(EmpVo empVo) throws BsnObjException {
		LOGGER.info("Inside the Business Method");
		try {
			return empDaoIfc.empInsrt(empVo);
		} catch (DataAcesExcp acesExcp) {
			throw new BsnObjException(acesExcp);
		}
		catch (ArithmeticException acesExcp) {
			throw new BsnObjException(acesExcp);
		}
	}

	@Override
	public List<EmpVo> getEmpVos() throws BsnObjException {
		LOGGER.info("Inside the Business Method");
		try {
			return empDaoIfc.getEmpVos();
		} catch (DataAcesExcp acesExcp) {
			throw new BsnObjException(acesExcp);
		}
	}

	@Override
	public Integer empUpdt(EmpVo empVo) throws BsnObjException {
		LOGGER.info("Inside the Business Method");
		try {
			return empDaoIfc.empUpdt(empVo);
		} catch (DataAcesExcp acesExcp) {
			throw new BsnObjException(acesExcp);
		}
	}

	@Override
	public EmpVo fndEmpById(Integer empId) throws BsnObjException {
		LOGGER.info("Inside the Business Method");
		try {
			return empDaoIfc.fndEmpById(empId);
		} catch (DataAcesExcp acesExcp) {
			throw new BsnObjException(acesExcp);
		}
	}

	@Override
	public Integer deleteEmp(Integer empId) throws BsnObjException {

		try {
			return empDaoIfc.deleteEmp(empId);
		} catch (DataAcesExcp acesExcp) {
			throw new BsnObjException(acesExcp);
		}
	}

}
