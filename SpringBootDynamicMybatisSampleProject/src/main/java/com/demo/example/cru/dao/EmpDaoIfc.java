package com.demo.example.cru.dao;

import java.util.List;

import com.demo.example.cru.excp.DataAcesExcp;
import com.demo.example.cru.vo.EmpVo;

public interface EmpDaoIfc {
	int empInsrt(EmpVo empVo) throws DataAcesExcp;

	List<EmpVo> getEmpVos() throws DataAcesExcp;

	int empUpdt(EmpVo empVo) throws DataAcesExcp;

	EmpVo fndEmpById(Integer empId) throws DataAcesExcp;

	Integer deleteEmp(Integer empId) throws DataAcesExcp;

}
