//package com.demo.example.cru.intgrtest;
//
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.web.WebAppConfiguration;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.MvcResult;
//
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.web.context.WebApplicationContext;
//
//import com.demo.example.Application;
//import com.demo.example.cru.vo.EmpVo;
//import com.fasterxml.jackson.databind.ObjectMapper;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(classes = Application.class)
//@WebAppConfiguration
//public class ApplicationIntgrTest {
//
//	@Autowired
//	private WebApplicationContext applicationContext;
//	private MockMvc mockMvc;
//
//	@Before
//	public void setUp() {
//		mockMvc = MockMvcBuilders.webAppContextSetup(applicationContext).build();
//	}
//
//	@Test
//	public void getEmpVosTest() throws Exception {
//		mockMvc.perform(get("/allemps")).andExpect(status().is(200));
//	}
//
//	@Test
//	public void getEmpVosTest_nullCheck() throws Exception {
//
//		MvcResult mvcResult = mockMvc.perform(get("/allemps")).andReturn();
//		Assert.assertNotNull(mvcResult.getResponse().getContentAsString());
//	}
//
//	@Test
//	public void fndEmpByIdTest() throws Exception {
//		mockMvc.perform(
//				get("/emp/{empId}", 1100).accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON))
//				.andExpect(status().is(200));
//	}
//
//	@Test
//	public void fndEmpByIdTest_nullCheck() throws Exception {
//		MvcResult mvcResult = mockMvc.perform(
//				get("/emp/{empId}", 1100).accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON))
//				.andReturn();
//		Assert.assertNotNull(mvcResult.getResponse().getContentAsString());
//	}
//
//	@Test
//	public void deleteEmpTest() throws Exception {
//		mockMvc.perform(
//				delete("/del/{empId}", 1234).accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON))
//				.andExpect(status().is(200));
//	}
//
//	@Test
//	public void deleteEmpTest_nullCheck() throws Exception {
//		MvcResult mvcResult = mockMvc.perform(
//				delete("/del/{empId}", 1234).accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON))
//				.andReturn();
//		Assert.assertNotNull(mvcResult.getResponse().getContentAsString());
//	}
//
//	@Test
//	public void empInsrtTest() throws Exception {
//		EmpVo empVo = new EmpVo();
//		empVo.setAddress("College Street");
//		empVo.setCity("Kol");
//		empVo.setEmplId(555);
//		empVo.setFname("Snehasish");
//		empVo.setLname("Guha Roy");
//		mockMvc.perform(post("/insert").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)
//				.content(asJsonString(empVo))).andExpect(status().is(200));
//
//	}
//
//	@Test
//	public void empInsrtTest_nullCheck() throws Exception {
//		EmpVo empVo = new EmpVo();
//		empVo.setAddress("College Street");
//		empVo.setCity("Kol");
//		empVo.setEmplId(6771);
//		empVo.setFname("Snehasish");
//		empVo.setLname("Guha Roy");
//		MvcResult mvcResult = mockMvc.perform(post("/insert").accept(MediaType.APPLICATION_JSON)
//				.contentType(MediaType.APPLICATION_JSON).content(asJsonString(empVo))).andReturn();
//
//		Assert.assertNotNull(mvcResult.getResponse().getContentAsString());
//	}
//
//	@Test
//	public void empUpdtTest() throws Exception {
//		EmpVo empVo = new EmpVo();
//		empVo.setAddress("Lake Town");
//		empVo.setCity("Kol");
//		empVo.setEmplId(301);
//		empVo.setFname("Ronny");
//		empVo.setLname("Das");
//		mockMvc.perform(put("/insert").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)
//				.content(asJsonString(empVo))).andExpect(status().is(200));
//
//	}
//
//	@Test
//	public void empUpdtTest_nullCheck() throws Exception {
//		EmpVo empVo = new EmpVo();
//		empVo.setAddress("Lake Town");
//		empVo.setCity("Kol");
//		empVo.setEmplId(301);
//		empVo.setFname("Ronny");
//		empVo.setLname("Das");
//		MvcResult mvcResult = mockMvc.perform(put("/insert").accept(MediaType.APPLICATION_JSON)
//				.contentType(MediaType.APPLICATION_JSON).content(asJsonString(empVo))).andReturn();
//		Assert.assertNotNull(mvcResult.getResponse().getContentAsString());
//
//	}
//
//	public static String asJsonString(final Object obj) {
//		try {
//			return new ObjectMapper().writeValueAsString(obj);
//		} catch (Exception e) {
//			throw new RuntimeException(e);
//		}
//	}
//
//}
